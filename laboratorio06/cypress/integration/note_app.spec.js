describe('Note app', () =>{
    //RUTAS DE LA PAGINA WEB 
    it('¿Acceso a la pagina de info?', () =>{
        cy.visit('http://localhost:8000/info')
        cy.contains('Phonebook has info for')
    })

    //GET
    it('¿Se puede de visualizar las personas?', () =>{
        cy.request('http://localhost:8000/api/persons').then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.length(4)
            expect(response).to.have.property('headers')
            expect(response).to.have.property('duration')
          })
    })

    it('¿Puede de ver personas unicas?', () =>{
        cy.request('http://localhost:8000/api/persons/1').then((response) => {
            expect(response.status).to.eq(200)
            expect(response).to.have.property('headers')
            expect(response).to.have.property('duration')
          })
    })

    //POST
    it('¿Creando un nuevo registro?', () =>{
        cy.request('POST','http://localhost:8000/api/persons', { name: 'Jane', number: "75-44-532352" }).then(
            (response) => {
              // Se espera que el registro sea encontrado una vez encontrado
              expect(response.body).to.have.property('name', 'Jane') // true
            })    
    })


    //DELETE
    it('¿Borrando un registro?', () =>{
        cy.request('DELETE', 'http://localhost:8000/api/persons/4')
    })
    

})