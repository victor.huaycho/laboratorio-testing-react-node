import supertest from 'supertest'
import app, { persons } from '../index'
import 'regenerator-runtime/runtime'

const testear = supertest(app)

const personasIniciales = [...persons]

beforeEach(() => {
  while (persons.length > 0) {
    persons.pop()
  }
  for (let i = 0; i < personasIniciales.length; i++) {
    persons.push(personasIniciales[i])
  }
})

test('devolucion de personas', async () => {
  await testear.get('/api/persons')
    .expect(200)
    .expect('Content-Type', /application\/json/)
})

test('Cantidad de personas = 4', async () => {
  const persons = await testear.get('/api/persons')
  expect(persons.body).toHaveLength(4)
})

test('Nombre de primera persona', async () => {
  const persons = await testear.get('/api/persons')
  expect(persons.body[0].name).toBe('Arto Hellas')
})

test('informacion de agenda', async () => {
  await testear.get('/info')
    .expect(200)
    .expect('Content-Type', 'text/html; charset=utf-8')
})

test('Obtener una persona', async () => {
  await testear.get('/api/persons/1')
    .expect(200)
    .expect('Content-Type', /application\/json/)
})

test('Obtener una persona - Persona no encontrada', async () => {
  await testear.get('/api/persons/5')
    .expect(404)
})

test('Eliminar una persona', async () => {
  await testear.delete('/api/persons/1')
    .expect(204)
})

test('Agregar Una persona', async () => {
  const newPerson = {
    name: 'David Alejo',
    number: '987654321'
  }
  await testear
    .post('/api/persons')
    .send(newPerson)
    .expect(200)
    .expect('Content-Type', /application\/json/)

  const response = await testear.get('/api/persons')
  
  const content = response.body.map(person => person.name)

  expect(response.body).toHaveLength(personasIniciales.length + 1)
  expect(content).toContain(newPerson.name)
})

test('Agregar persona - Campos en blanco', async () => {
  const newPerson = {
    name: '',
    number: ''
  }
  await testear
    .post('/api/persons')
    .send(newPerson)
    .expect(406)
    .expect('Content-Type', /application\/json/)
})

test('Agregar persona - Campos de Nombre ya existentes', async () => {
  const newPerson = {
    name: 'Arto Hellas',
    number: '4654561322313'
  }
  await testear
    .post('/api/persons')
    .send(newPerson)
    .expect(406)
    .expect('Content-Type', /application\/json/)
})

test('Ruta no existente', async () => {
  await testear.get('/api/persons/rutanoexistente')
    .expect(404)
})
