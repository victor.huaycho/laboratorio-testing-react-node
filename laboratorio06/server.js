import app from './index'

const PORT = 8000 // process.env.PORT

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})
