import React from "react"
import '@testing-library/jest-dom/extend-expect'
import { render } from "@testing-library/react"
import Person from "./Person"

test('datos renderizados correctamente', () => {

    const person = {
        name: 'Raul',
        number: '9658421857'
    }

    const component = render(<Person person={person} />)

    expect(component.container).toHaveTextContent(person.name)
    expect(component.container).toHaveTextContent(person.number)
})