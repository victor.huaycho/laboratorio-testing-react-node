import React from "react"
import '@testing-library/jest-dom/extend-expect'
import { fireEvent, render } from "@testing-library/react"
import Filter from "./Filter"

const setup = () => {
    const mockHandler = jest.fn()
    const utils = render(<Filter />)
    const input = utils.getByLabelText('cost-input')
    return {
      input,
      ...utils,
    }
  }

test('Filtro funcional', () => {

    const { input } = setup()
    fireEvent.change(input, { target: { value: 'nuevo' } })
    expect(input.value).toBe('nuevo')

})