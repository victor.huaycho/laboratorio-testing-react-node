describe('Persons app', () =>{
    //Funcionalidades
    beforeEach(() => {
        cy.visit('http://localhost:3000/')
    })
    it('¿Acceso a la pagina de inicio?', () =>{       
        cy.contains('Phonebook')
    })

    it('¿ Boton de "add" ?', () =>{
        cy.contains('add').click()
    })

    it('Prueba de llenado de campos y busquedas', () =>{
        cy.get('[name="name"]').type('Pedro Castillo')
        cy.get('[name="number"]').type('155-215-215')
        cy.get('#save-form').click()
    })

    afterEach(() => {
        cy.get('[name="name"]').type('Luan Cancervero')
        cy.get('[name="number"]').type('452-2156-647')
        cy.get('#save-form').click()
        cy.contains('Numbers')  
    })

    afterEach(() => {
        cy.get('[name="search"]').type('Pedro')
        cy.contains('Numbers')  
    })
})